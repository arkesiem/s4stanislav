#include <iostream>
#include <iomanip>
#include <sstream>

void displayIndexes(std::ostream &s, int number, int width);
void displayColumns(int columns, int width);
void createMatrix(float **&matrix, int &rows, int &columns);
void displayColumns(int columns, int width);
void displayMatrix(float **&matrix, int &rows, int &columns, int width);
void getMatrixValues(float **&matrix, int&rows, int &columns);

int main() {
	const int width = 10;
	int rows, columns;
	float **matrix;
	createMatrix(matrix, rows, columns);
	displayMatrix(matrix, rows, columns, width);
	return 0;
}

void createMatrix(float **&matrix , int &rows, int &columns) {

	std::cout << "Give the number or rows";
	std::cin >> rows;
	std::cout << "Give the number of columns";
	std::cin >> columns;

	matrix = new float*[rows];
	for (int i = 0; i < rows; i++) {
		matrix[i] = new float[columns];
	}
	getMatrixValues(matrix, rows, columns);
}

void getMatrixValues(float **&matrix, int&rows, int &columns) {

	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < columns; j++) {
			std::cout << "Input [" << i << "][" << j << "]: " << std::endl;
			std::cin >> matrix[i][j];
		}
	}
}

void displayColumns(int columns, int width) {

	std::cout << std::setw(width) << "";
	for (int i = 0; i < columns; i++) {
		displayIndexes(std::cout, i, width);
	}
	std::cout << std::endl;
}

void displayMatrix(float **&matrix, int &rows, int &columns, int width) {

	displayColumns(columns, width);
	for (int i = 0; i < rows; i++) {
		displayIndexes(std::cout, i, width);
		for (int j = 0; j < columns; j++) {
			if (matrix[i][j] == 0) {
				std::cout << std::setw(width) << "";
			}
			else {
				std::cout << std::setprecision(3) << std::setw(width) << matrix[i][j];
			}
		}
		std::cout << std::endl;
	}
}

void displayIndexes(std::ostream &s, int number, int width) {

	std::stringstream string;

	string << "[" << number << "]";

	s << std::setw(width) << string.str();
}

