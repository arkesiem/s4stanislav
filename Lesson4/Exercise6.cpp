#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

void getNumberOfLines(int &numberOfLines);
void displayLastLines(std::stringstream &fileName, int &numberOfLines);
void getFileName(std::stringstream &fileName);
void checkFile(std::ifstream &file);

int main() {
	std::stringstream fileName;
	int numberOfLines;
	displayLastLines(fileName, numberOfLines);
	return 0;
}

void getNumberOfLines(int &numberOfLines) {
	std::cout << "Input number of lines you want to read: " << std::endl;
	std::cin >> numberOfLines;
}
void displayLastLines(std::stringstream &fileName, int &numberOfLines) {
	getNumberOfLines(numberOfLines);
	getFileName(fileName);
	std::ifstream file(fileName.str());
	std::string line;
	std::vector<std::string> lines;
	checkFile(file);
	file.seekg(0, std::ios::end);
	if ((int)file.tellg() == 0) {
		std::cout << "File is empty!" << std::endl;
		exit(0);
	}
	file.seekg(0, std::ios::beg);
	while(!file.eof()) {
		std::getline(file, line);
		lines.push_back(line);
	}
	for (int i = lines.size() - 1; i >= (lines.size() - numberOfLines); i--) {
		std::cout << lines[i] << std::endl;
	}
}
void getFileName(std::stringstream &fileName) {

	std::string name;
	std::cout << "Input file name: " << std::endl;
	std::cin >> name;
	fileName << name << ".txt";
}

void checkFile(std::ifstream &file) {
	if (!file.is_open()) {
		std::cerr << "File not found!" << std::endl;
		exit(0);
	}
}