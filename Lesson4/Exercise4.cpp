#include <iostream>
#include <fstream>
#include <sstream>


void getFileName(std::stringstream &fileName);
void checkFileExistence(std::ifstream &file);
void printFileContent(std::stringstream &fileName);;

int main() {
	std::stringstream fileName;
	getFileName(fileName);
	printFileContent(fileName);
	return 0;
}

void getFileName(std::stringstream &fileName) {
	
	std::string name;
	const char *extension = ".txt";
	std::cout << "Input filename:" << std::endl;
	std::getline(std::cin, name);
	fileName << name << extension;
}

void checkFileExistence(std::ifstream &file) {
	if (!file.is_open()) {
		std::cerr << "Your file name was not found or given file extension is invalid!" << std::endl;
		exit(0);
	}
}
void printFileContent(std::stringstream &fileName) {
	
	std::ifstream file(fileName.str());
	std::string line;
	checkFileExistence(file);
	while (!file.eof()) {
		std::getline(file, line);
		std::cout << line << std::endl;
	}
}